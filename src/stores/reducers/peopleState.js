import * as actionTypes from './../../constant/actiontypes';

const initialState = {
    count: 0,
    list: [],
    nextUrl: '',
    prevUrl: ''
};

export default function peopleStateReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.ADD_PEOPLE:
            return Object.assign({}, state, {
                list: state.list.concat(action.people)
            });
        case actionTypes.SET_PEOPLE_LIST:
            return Object.assign({}, state, {
                list: action.people
            })
        case actionTypes.SELECT_PEOPLE:
            return Object.assign({}, state, {
                list: state.list.map(people => {
                    return people.ID == action.peopleId ?
                        Object.assign({}, people, { isSelected: true }) :
                        Object.assign({}, people, { isSelected: false })
                })
            })
        case actionTypes.UNSELECT_ALL_PEOPLE:
            return Object.assign({}, state, {
                list: state.list.map(people => {
                    return Object.assign({}, people, { isSelected: false })
                })
            })
        case actionTypes.SET_NEXT_URL:
            return Object.assign({}, state, {
                nextUrl: action.nextUrl
            });
        case actionTypes.SET_PREV_URL:
            return Object.assign({}, state, {
                prevUrl: action.prevUrl
            });
        case actionTypes.SET_PEOPLE_COUNT:
            return Object.assign({}, state, {
                count: action.count
            });
        default: return state;
    }
}