import * as actionTypes from './../../constant/actiontypes';

const initialState = {
    show: false,
    title: '',
    content: ''
};

export default function modalStateReducer(state = initialState, action) {
    switch (action.type) {
        case actionTypes.SET_MODAL:
            return Object.assign({}, state, {
                show: action.show,
                title: action.title,
                content: action.content
            });
        case actionTypes.HIDE_MODAL:
            return Object.assign({}, state, {
                show: false
            });
        default: return state;
    }
}