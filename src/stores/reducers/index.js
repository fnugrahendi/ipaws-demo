import { combineReducers } from 'redux';
import modalState from './modalState';
import peopleState from './peopleState';

const rootReducer = combineReducers({
    modalState,
    peopleState
});

export default rootReducer