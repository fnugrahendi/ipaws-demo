import * as actionTypes from './../../constant/actiontypes';

export function addPeople(people) {
    return ({ type: actionTypes.ADD_PEOPLE, people });
}

export function setPeopleList(people) {
    return ({ type: actionTypes.SET_PEOPLE_LIST, people })
}

export function selectPeople(peopleId) {
    return ({ type: actionTypes.SELECT_PEOPLE, peopleId })
}

export function unselectAllPeople() {
    return ({ type: actionTypes.UNSELECT_ALL_PEOPLE })
}

export function setPeopleCount(count) {
    return ({ type: actionTypes.SET_PEOPLE_COUNT, count });
}

export function setNextUrl(nextUrl) {
    return ({ type: actionTypes.SET_NEXT_URL, nextUrl });
}

export function setPrevUrl(prevUrl) {
    return ({ type: actionTypes.SET_PREV_URL, prevUrl });
}