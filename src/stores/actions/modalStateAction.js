import * as actionTypes from './../../constant/actiontypes';

export function setModal(modal) {
    return ({
        type: actionTypes.SET_MODAL,
        show: modal.show,
        title: modal.title,
        content: modal.content
    });
}

export function hideModal() {
    return ({
        type: actionTypes.HIDE_MODAL
    })
}