import axios from 'axios';

const rootURL = 'https://swapi.co/api';

export const getAllPeople = () => {
    const peopleApiURL = `${rootURL}/people/`;
    return getDataByUrl(peopleApiURL);
}

export const getAllFilms = () => {
    const filmApiURL = `${rootURL}/films/`;
    return getDataByUrl(filmApiURL);
}

export const getAllPlanets = () => {
    const planetApiURL = `${rootURL}/planets/`;
    return getDataByUrl(planetApiURL);
}

export const getAllSpecies = () => {
    const speciesApiURL = `${rootURL}/species/`;
    return getDataByUrl(speciesApiURL);
}

export const getAllStarships = () => {
    const starshipsApiURL = `${rootURL}/starships/`;
    return getDataByUrl(starshipsApiURL);
}

export const getAllVehicles = () => {
    const vehicleApiURL = `${rootURL}/vehicles/`;
    return getDataByUrl(vehicleApiURL);
}

export const getDataByUrl = (url) => {
    return axios.get(url);
}