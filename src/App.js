import React, { Component } from 'react';
import Header from './component/Header';
import Menu from './component/Menu';
import Content from './component/Content';
import './App.css';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Header>
          <Menu/>
        </Header>
        <Content />
      </div>
    );
  }
}