import React from 'react';
import { Link } from 'react-router-dom';

export default () => {
    return (
        <div className='menu'>
            <Link to={'/'}>People</Link>
            <Link to={'/films'}> Films </Link>
            <Link to={'/planets'}>Planets</Link>
            <Link to={'/species'}>Species</Link>
            <Link to={'/starships'}>Starships</Link>
            <Link to={'/vehicles'}>Vehicles</Link>
        </div>
    );
}