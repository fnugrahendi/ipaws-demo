import React, { Fragment, Component } from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

export default class ListView extends Component {
    render() {
        return (
            <Fragment>
                {this.props.data.map((datum, id) => {
                    return (
                        <ListItem
                            button
                            onClick={() => this.props.selectItem(datum.url)}
                            style={{ textAlign: 'left', margin: '5px 10px', cursor: 'pointer' }}
                            key={id}>
                            <ListItemText>{datum.name ? datum.name : datum.title}</ListItemText>
                        </ListItem>
                    )
                })}
            </Fragment>
        );
    }
}