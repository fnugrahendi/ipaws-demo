import React, { Component } from 'react';
import { Route } from 'react-router-dom';
import HOCLoader from './HOCLoader';
import ListView from './ListView';
import ModalWrapper from './ModalWrapper';
import DetailView from './DetailView';
import {
    getAllPeople,
    getAllFilms,
    getAllPlanets,
    getAllSpecies,
    getAllStarships,
    getAllVehicles,
    getDataByUrl
} from './../api/swApi';

const PeopleListWithLoader = HOCLoader(getAllPeople)(ListView);
const FilmsListWithLoader = HOCLoader(getAllFilms)(ListView);
const PlanetListWithLoader = HOCLoader(getAllPlanets)(ListView);
const SpeciesListWithLoader = HOCLoader(getAllSpecies)(ListView);
const StarshipListWithLoader = HOCLoader(getAllStarships)(ListView);
const VehicleListWithLoader = HOCLoader(getAllVehicles)(ListView);

export default class Content extends Component {
    state = {
        showModal: false,
        modalContent: '',
        title: 'Detail'
    }
    onCloseModal = () => {
        if (this.state.showModal)
            this.setState({ showModal: false });
    }
    onOpenModal = (content) => {
        this.setState({
            showModal: true,
            modalContent: content
        });
    }
    selectItem = async (url) => {
        let data = await getDataByUrl(url);
        let detailData = <DetailView data={data.data} />
        this.onOpenModal(detailData)
    }
    render() {
        const { showModal, modalContent, title } = this.state;
        return (
            <div className={'content'}>
                <Route exact path='/' render={(props) => <PeopleListWithLoader {...props} selectItem={this.selectItem}/>} />
                <Route path='/films' render={(props) => <FilmsListWithLoader {...props} selectItem={this.selectItem}/>} />
                <Route path='/planets' render={(props) => <PlanetListWithLoader {...props} selectItem={this.selectItem}/>} />
                <Route path='/species' render={(props) => <SpeciesListWithLoader {...props} selectItem={this.selectItem}/>} />
                <Route path='/starships' render={(props) => <StarshipListWithLoader {...props} selectItem={this.selectItem}/>} />
                <Route path='/vehicles' render={(props) => <VehicleListWithLoader {...props} selectItem={this.selectItem}/>} />
                <ModalWrapper
                    show={showModal}
                    onClose={this.onCloseModal}
                    content={modalContent}
                    title={title} />
            </div>
        );
    }
}