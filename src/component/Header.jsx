import React from 'react';

export default (props) => {
    return (
        <React.Fragment>
            <header className="App-header">
                SW API
            </header>
            {props.children}
        </React.Fragment>
    );
}