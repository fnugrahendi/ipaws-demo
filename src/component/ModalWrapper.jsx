import React from 'react';

import { Modal, Typography, Paper } from '@material-ui/core';

const style = {
    position: 'absolute',
    width: '500px',
    height: 'fit-content',
    backgroundColor: 'white',
    padding: '10px',
    top: '30%',
    left: '34%'
};

class ModalWrapper extends React.Component {
    render() {
        const { show, onClose, title, content } = this.props;
        return (
            <Modal
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={show}
                onClose={onClose}
                style={style}
            >
                <Paper>
                    <h5 style={{ padding: '15px 25px 0' }}>{title}</h5>
                    {content}
                </Paper>
            </Modal>
        )
    }
}

export default ModalWrapper;