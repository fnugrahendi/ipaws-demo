import React, { Fragment, Component } from 'react';

import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import FaceIcon from '@material-ui/icons/Face';

export default class PeopleList extends Component {
    render() {
        return (
            <Fragment>
                {this.props.peopleList.map((people, id) => {
                    return (
                        <ListItem 
                            button
                            onClick={() => this.props.onPeopleClick(id)}
                            style={{textAlign: 'left', margin: '5px 10px', cursor: 'pointer'}} 
                            key={id}>
                            <Avatar><FaceIcon/></Avatar>
                            <ListItemText>{people.name}</ListItemText>
                        </ListItem>
                    )
                })}
            </Fragment>
        );
    }
}