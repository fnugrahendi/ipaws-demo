import React, { Component, Fragment } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { find } from 'lodash';

import { List, Button } from '@material-ui/core';

import * as PeopleStateAction from './../stores/actions/peopleStateAction';
import * as ModalStateAction from './../stores/actions/modalStateAction';

import * as swapi from './../api/swApi';
import PeopleList from './PeopleList';
import PeopleDetail from './PeopleDetail';
import ModalWrapper from './ModalWrapper';
import LoadingView from './LoadingView';

const mapStateToProps = (state) => {
    return {
        modalState: state.modalState,
        peopleState: state.peopleState
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        modalStateAction: bindActionCreators(ModalStateAction, dispatch),
        peopleStateAction: bindActionCreators(PeopleStateAction, dispatch)
    }
}

class PeopleListContainer extends Component {
    state = {
        showLoading: false,
        loadingText: ''
    }
    async componentDidMount() {
        const { peopleStateAction } = this.props;
        this.showLoading('loading the rebellion army ...');
        const response = await swapi.getAllPeople();
        const peopleData = await response.data.results;
        peopleStateAction.setPeopleCount(response.data.count);
        peopleStateAction.setNextUrl(response.data.next);
        peopleStateAction.setPrevUrl(response.data.previous);
        this.addPeopleFromList(peopleData);
        this.hideLoading();
    }

    loadPeopleDataByUrl = async (url) => {
        const { peopleStateAction } = this.props;
        this.showLoading('loading the rebellion army ...');
        const response = await swapi.getDataByUrl(url);
        const peopleData = await response.data.results;
        peopleStateAction.setPeopleCount(response.data.count);
        peopleStateAction.setNextUrl(response.data.next);
        peopleStateAction.setPrevUrl(response.data.previous);
        this.replacePeopleList(peopleData);
        this.hideLoading();
    }

    peopleDataParser = (peopleData) => {
        const peopleList = [];
        peopleData.map((people, ID) => {
            return peopleList.push(
                {
                    ID: ID,
                    name: people.name,
                    url: people.url,
                    birthYear: people.birth_year,
                    created: people.created,
                    edited: people.edited,
                    eyeColor: people.eye_color,
                    gender: people.gender,
                    hairColor: people.hair_color,
                    height: people.height,
                    mass: people.mass,
                    skinColor: people.skin_color,
                    isSelected: false
                }
            )
        });
        return peopleList;
    }

    replacePeopleList = (peopleData) => {
        const peopleList = this.peopleDataParser(peopleData);
        this.props.peopleStateAction.setPeopleList(peopleList);
    }

    addPeopleFromList = (peopleData) => {
        const peopleList = this.peopleDataParser(peopleData);
        this.props.peopleStateAction.addPeople(peopleList);
    }

    selectPeople = (peopleId) => {
        const { peopleState, peopleStateAction, modalStateAction } = this.props;
        this.showLoading('loading the force ...');
        peopleStateAction.selectPeople(peopleId);
        const selectedPeople = find(peopleState.list, function (people) { return people.ID == peopleId; });
        if (selectedPeople) {
            const content =
                <PeopleDetail
                    {...selectedPeople}
                />
            modalStateAction.setModal({
                show: true,
                title: selectedPeople.name,
                content: <Fragment>{content}</Fragment>
            });
        }
        this.hideLoading();
    }

    modalCloseHandler = () => {
        this.props.modalStateAction.hideModal();
    }

    showLoading = (text) => {
        this.setState({
            showLoading: true,
            loadingText: text
        })
    }

    hideLoading = () => {
        this.setState({
            showLoading: false
        })
    }

    render() {
        const { modalState, peopleState } = this.props;
        return (
            <Fragment>
                <LoadingView
                    show={this.state.showLoading}
                    text={this.state.loadingText}
                />
                <ModalWrapper modalState={modalState} modalCloseHandler={this.modalCloseHandler}/>
                <Fragment>
                    <List style={{ width: '500px', display: 'inline-block', maxHeight: '640px', overflowY: 'auto', overflowX: 'hidden' }}>
                        <PeopleList
                            peopleList={this.props.peopleState.list}
                            onPeopleClick={this.selectPeople}
                        />
                    </List>
                    <div>
                        <Button variant="contained" color="primary" disabled={!peopleState.prevUrl} onClick={() => this.loadPeopleDataByUrl(peopleState.prevUrl)}>Prev</Button>
                        <span> | </span>
                        <Button variant="contained" color="primary" disabled={!peopleState.nextUrl} onClick={() => this.loadPeopleDataByUrl(peopleState.nextUrl)}>Next</Button>
                    </div>
                </Fragment>
            </Fragment>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(PeopleListContainer);