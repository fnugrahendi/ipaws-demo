import React from 'react';

export default class PeopleDetail extends React.Component {
    render() {
        const { name, birthYear, eyeColor, gender,
            hairColor, height, mass, skinColor, created } = this.props;
        return (
            <div style={{ textAlign: 'left', margin: '5px 10px', cursor: 'pointer', padding: '20px' }}>
                <div>Name: {name}</div>
                <div>Birth Year: {birthYear}</div>
                <div>Eye Color: {eyeColor}</div>
                <div>Gender: {gender}</div>
                <div>Hair Color: {hairColor}</div>
                <div>Height: {height}</div>
                <div>Mass: {mass}</div>
                <div>Skin Color: {skinColor}</div>
                <div style={{ opacity: '0.5', fontSize: '12px', marginTop: '10px' }}>Created at {created}</div>
            </div>
        )
    }
}