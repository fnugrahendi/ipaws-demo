import React from 'react';

const DetailView = (props) => {
    return (
        <div style={{ textAlign: 'left', margin: '5px 10px', cursor: 'pointer', padding: '20px' }}>
            {
                Object.keys(props.data).map((dataKey, id) => {
                    return <div key={`${dataKey}_${id}`}>{dataKey} : {props.data[dataKey]}</div>
                })
            }
        </div>
    )
}

export default DetailView;