import React from 'react';
import PropTypes from 'prop-types';
import './LoadingView.css';

const LoadingView = (props) => (
    <div className={`loading-view ${props.show ? 'show' : 'hide'} ${props.mode}`}>
        <div className={'lds-roller'}>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
        <div className={'loading-text'}>
            {props.text}
        </div>
    </div>
);

export default LoadingView;

LoadingView.propTypes = {
    text: PropTypes.string,
    show: PropTypes.bool,
    mode: PropTypes.oneOf([
        'FullScreen',
        'Contained'
    ])
};

LoadingView.defaultProps = {
    text: 'Loading the force ...',
    show: false,
    mode: 'FullScreen'
};