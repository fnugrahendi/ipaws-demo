import React, {Component} from 'react';
import LoadingView from './LoadingView';

const HOCLoader = (fetchData) => (WrappedComponent) => {
    return class HOCLoader extends Component {
        state = {
            data: []
        };
        async componentDidMount() {
            try {
                let data = await fetchData();
                console.log(data.data);
                this.setState({ data: data.data.results });
            } catch (error) {
                console.log(error);
            }
        }
        render() {
            return (
                this.state.data && this.state.data.length > 0 ?
                <WrappedComponent data={this.state.data} {...this.props}/> :
                <LoadingView show={true} text={' loading the force ... '} />
            )
        }
    }
}

export default HOCLoader;