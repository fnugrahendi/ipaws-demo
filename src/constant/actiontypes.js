export const ADD_PEOPLE = 'ADD_PEOPLE';
export const SET_PEOPLE_LIST = 'SET_PEOPLE_LIST';
export const SELECT_PEOPLE = 'SELECT_PEOPLE';
export const UNSELECT_ALL_PEOPLE = 'UNSELECT_ALL_PEOPLE';
export const SET_NEXT_URL = 'SET_NEXT_URL';
export const SET_PREV_URL = 'SET_PREV_URL';
export const SET_PEOPLE_COUNT = 'SET_PEOPLE_COUNT';

export const SET_MODAL = 'SET_MODAL';
export const HIDE_MODAL = 'HIDE_MODAL';
